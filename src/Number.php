<?php
namespace Examples;

class Number
{
	/**	
	 * @param $number
	 * @param $rangestart
	 * @param $rangeEnd
	 */
	public static function isBetween($number, $rangeStart, $rangeEnd)
	{
		if ($number < $rangeStart || $number > $rangeEnd) {
			return false;
		}
		
		return true;
	}
}