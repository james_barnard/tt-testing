<?php

use Examples\Number;
use Codeception\Test\Unit;

class NumberHelperTest extends Unit
{
    /**
     * @dataProvider validNumberProvider
     * @param $number
     * @param $rangeStart
     * @param $rangeEnd
     */
    public function testIfIsBetweenReturnsTrueWhenPassedInVariablesAreValid($number, $rangeStart, $rangeEnd)
    {
//        $number = 42;
//        $rangeStart = 27;
//        $rangeEnd = 101;
        $this->assertTrue(Number::isBetween($number, $rangeStart, $rangeEnd));
    }
    
    public function testIfIsBetweenReturnsFalseWhenPassedInNumberIsntBetween()
    {
    	$number = 27;
        $rangeStart = 42;
        $rangeEnd = 101;
        $this->assertFalse(Number::isBetween($number, $rangeStart, $rangeEnd));
    }

    public function validNumberProvider()
    {
        return [
            [2,1,4],
            [1,1,4],
            [4,1,4]
        ];
    }
}