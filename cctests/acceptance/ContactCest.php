<?php

class ContactCest
{
    public function _before(AcceptanceTester $I)
    {

    }

    public function _after(AcceptanceTester $I)
    {

    }

    //tests
    public function basicContactFormVisitAndFormSubmissionTest(AcceptanceTester $I)
    {
        /**
         * A very simple acceptance test with codeception
         */
        $I->wantTo('Visit the home page, see the Hello, World!, click the contact link, fill the form and see the success page');
        $I->amOnPage('/index.html');
        $I->see('Hello, World!');
        $I->click('Contact');
        $I->see('Contact');

        // fill in the contact form
        $I->fillField('name', 'James Barnard');
        $I->fillField('email', 'james.barnard@readingroom.com');
        $I->click('#contact-us-form input[type="submit"]');

        $I->see('We will be in touch');
    }
}